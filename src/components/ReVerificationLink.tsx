import { NavigationProp, useNavigation } from '@react-navigation/native';
import AppLink from '@ui/AppLink';
import colors from '@utils/colors';
import {FC, useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import { useSelector } from 'react-redux';
import { ProfileNavigatorStackParamList } from 'src/@types/navigation';
import { getClient } from 'src/api/client';
import { getAuthState } from 'src/store/auth';

interface Props {
    time?: number;
    activeAtFirst?: boolean;
    linkTitle: string;
    userId?: string;
}

const ReVerificationLink: FC<Props> = ({time=30,linkTitle,userId,activeAtFirst=false}) => {
  const [countdown, setCountdown] = useState(30);
  const [canSendNewOtpReq, setCanSendNewOtpReq] = useState(activeAtFirst);
  const {profile} = useSelector(getAuthState);
  const {navigate} = useNavigation<NavigationProp<ProfileNavigatorStackParamList>>();

  const resquestNewOtp = async () => {
    setCountdown(30);
    setCanSendNewOtpReq(false);
    try {
      const client = await getClient();
      await client.post('/auth/re-verify-email', {userId: userId || profile?.id});
      navigate('Verification', {userInfo: {
        email: profile?.email || '',
        name: profile?.name || '',
        id: userId || profile?.id || ''
      }});
    } catch(error) {
      console.log('Error when requesting new OTP: ', error);
    }
  }

  useEffect(() => {
    if(canSendNewOtpReq) return;

    const interval = setInterval(() => {
        setCountdown((oldCountdown) => {
          if(oldCountdown <= 0) {
            setCanSendNewOtpReq(true);
            clearInterval(interval);
            return 0;
          }
          return oldCountdown-1;
        });
    }, 1000);

    return () => {
      clearInterval(interval);
    }
  }, [canSendNewOtpReq]);

  return <View style={styles.container}>
    <View style={styles.container}>
        {countdown >0 && !canSendNewOtpReq ? <Text style={styles.countdown}>{countdown} sec</Text> : null}
        <AppLink active={canSendNewOtpReq} link={linkTitle} onPress={resquestNewOtp}/>
      </View>
  </View>;
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    countdown: {
        color: colors.SECONDARY,
        marginRight: 7
      }
});

export default ReVerificationLink;