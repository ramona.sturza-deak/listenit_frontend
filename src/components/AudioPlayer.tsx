import AppLink from '@ui/AppLink';
import AppModal from '@ui/AppModal';
import colors from '@utils/colors';
import {FC, useState} from 'react';
import {StyleSheet, View, Image, Text, Pressable} from 'react-native';
import {useProgress} from 'react-native-track-player';
import {useDispatch, useSelector} from 'react-redux';
import {getPlayerState, updatePlaybackRate} from 'src/store/player';
import formatDuration from 'format-duration';
import Slider from '@react-native-community/slider';
import useAudioController from 'src/hooks/useAudioController';
import AntDesign from 'react-native-vector-icons/AntDesign';
import PlayPauseButton from '@ui/PlayPauseButton';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import PlayerControler from '@ui/PlayerControler';
import Loader from '@ui/Loader';
import PlaybackSelector from '@ui/PlaybackSelector';
import AudioInfoContainer from './AudioInfoContainer';
import MaterialComIcon from 'react-native-vector-icons/MaterialCommunityIcons';

interface Props {
  visible: boolean;
  onRequestClose(): void;
  onListOptionPress?(): void;
  onProfileLinkPress?(): void;
}

const formttedDuration = (duration = 0) => {
  return formatDuration(duration, {
    leading: true /*to show 01, not 1*/,
  });
};

const AudioPlayer: FC<Props> = ({visible, onRequestClose, onListOptionPress, onProfileLinkPress}) => {
  const {onGoingAudio, playbackRate} = useSelector(getPlayerState);
  const {
    isPlaying,
    isBusy,
    seekTo,
    skipTo,
    togglePlayPause,
    onNextPress,
    onPreviousPress,
    setPlaybackRate,
  } = useAudioController();
  const [showAudioInfo, setShowAudioInfo] = useState(false);
  const poster = onGoingAudio?.poster;
  const source = poster ? {uri: poster} : require('../assets/music.png');
  const {duration, position} = useProgress();
  const dispatch = useDispatch();
  const updateSeek = async (value: number) => {
    await seekTo(value);
  };
  const handleSkipTo = async (skipType: 'forward' | 'reverse') => {
    if (skipType === 'forward') await skipTo(10);
    if (skipType === 'reverse') await skipTo(-10);
  };
  const handleNextPress = async () => {
    await onNextPress();
  };
  const handlePreviousPress = async () => {
    await onPreviousPress();
  };
  const onPlaybackRatePress = async (rate: number) => {
    await setPlaybackRate(rate);
    dispatch(updatePlaybackRate(rate));
  };

  return (
    <AppModal animation visible={visible} onRequestClose={onRequestClose}>
      <View style={styles.container}>
        <Pressable onPress={() => setShowAudioInfo(true)} style={styles.infoButton}>
          <AntDesign name="infocirlceo" color={colors.CONTRAST} size={37} />
        </Pressable>
        <AudioInfoContainer visible={showAudioInfo} closeHandler={setShowAudioInfo}/>
        <Image source={source} style={styles.poster} />
        <View style={styles.contentContainer}>
          <Text style={styles.title}>{onGoingAudio?.title}</Text>
          <AppLink onPress={onProfileLinkPress} link={onGoingAudio?.owner.name || ''} />
          <View style={styles.durationContainer}>
            <Text style={styles.duration}>
              {formttedDuration(position * 1000)}
            </Text>
            <Text style={styles.duration}>
              {formttedDuration(duration * 1000)}
            </Text>
          </View>
          <Slider
            minimumValue={0}
            maximumValue={duration}
            minimumTrackTintColor={colors.CONTRAST}
            maximumTrackTintColor={colors.INACTIVE_CONTRAST}
            value={position}
            onSlidingComplete={updateSeek}
          />
          <View style={styles.controls}>
            <PlayerControler onPress={handlePreviousPress} ignoreContainer>
              <AntDesign
                name="stepbackward"
                size={24}
                color={colors.CONTRAST}
              />
            </PlayerControler>
            <PlayerControler
              onPress={() => handleSkipTo('reverse')}
              ignoreContainer>
              <FontAwesome
                name="rotate-left"
                size={18}
                color={colors.CONTRAST}
              />
              <Text style={styles.skipText}>-10s</Text>
            </PlayerControler>
            <PlayerControler>
              {isBusy ? (
                <Loader color={colors.PRIMARY} />
              ) : (
                <PlayPauseButton
                  playing={isPlaying}
                  onPress={togglePlayPause}
                  color={colors.PRIMARY}
                />
              )}
            </PlayerControler>
            <PlayerControler
              onPress={() => handleSkipTo('forward')}
              ignoreContainer>
              <FontAwesome
                name="rotate-right"
                size={18}
                color={colors.CONTRAST}
              />
              <Text style={styles.skipText}>+10s</Text>
            </PlayerControler>
            <PlayerControler onPress={handleNextPress} ignoreContainer>
              <AntDesign name="stepforward" size={24} color={colors.CONTRAST} />
            </PlayerControler>
          </View>
          <PlaybackSelector
            onPress={onPlaybackRatePress}
            activeRate={playbackRate.toString()}
            containerStyle={{marginTop: 20}}
          />
          <View style={styles.listOptionContainer}>
            <PlayerControler ignoreContainer onPress={onListOptionPress}>
              <MaterialComIcon name='playlist-music' size={24} color={colors.CONTRAST}/>
            </PlayerControler>
          </View>
        </View>
      </View>
    </AppModal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 20,
  },
  poster: {
    marginTop: 50,
    width: 200,
    height: 200,
    borderRadius: 10,
  },
  contentContainer: {
    width: '100%',
    flex: 1,
    marginTop: 20,
  },
  title: {
    fontSize: 18,
    fontWeight: '700',
    color: colors.CONTRAST,
  },
  durationContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  duration: {
    color: colors.CONTRAST,
  },
  controls: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  skipText: {
    fontSize: 12,
    marginTop: 2,
    color: colors.CONTRAST,
  },
  infoButton: {
    position: 'absolute',
    right: 20,
    top: 10,
  },
  listOptionContainer: {
    alignItems: 'flex-end'
  }
});

export default AudioPlayer;