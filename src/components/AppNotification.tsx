import colors from '@utils/colors';
import {FC, useEffect} from 'react';
import {StyleSheet, Text} from 'react-native';
import Animated, { useAnimatedStyle, useSharedValue, withTiming } from 'react-native-reanimated';
import { useDispatch, useSelector } from 'react-redux';
import { getNotificationState, updateNotification } from 'src/store/notification';

interface Props {}

const AppNotification: FC<Props> = (props) => {
  const {message, type} = useSelector(getNotificationState);
  const height = useSharedValue(0);
  const dispatch = useDispatch();

  const heightStyle = useAnimatedStyle(() => {
    return {
        height: height.value
    }
  })

  let backgrColor = colors.ERROR;
  let textColor = colors.CONTRAST;

  switch(type) {
    case 'success':
        backgrColor = colors.SUCCESS;
        textColor = colors.PRIMARY;
        break;
  }

  useEffect(() => {
    const performAnimation = async () => {
        height.value = await withTiming(45, {
            duration: 150
        });

        const timeoutId = setTimeout(async () => {
            height.value = await withTiming(0, {
                duration: 150
            });
            dispatch(updateNotification({message: '', type}));
        }, 3000);

        return () => {
            clearTimeout(timeoutId);
        };
    };

    if(message) performAnimation();
}, [message]);

  return <Animated.View style={[styles.container, {backgroundColor: backgrColor}, heightStyle]}>
    <Text style={[styles.message, {color: textColor}]}>{message}</Text>
  </Animated.View>;
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    message: {
        fontSize: 18,
        alignItems: 'center'
    }
});

export default AppNotification;