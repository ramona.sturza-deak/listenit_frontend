import AvatarField from '@ui/AvatarField';
import colors from '@utils/colors';
import {FC} from 'react';
import {StyleSheet, View, Text, Pressable} from 'react-native';
import { UserProfile } from 'src/store/auth';
import MaterialIcon from  'react-native-vector-icons/MaterialIcons';
import AntDesign from  'react-native-vector-icons/AntDesign';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import { ProfileNavigatorStackParamList } from 'src/@types/navigation';
import Octicons from 'react-native-vector-icons/Octicons';

interface Props {
    profile?: UserProfile | null; 
}

const ProfileContainer: FC<Props> = ({profile}) => {
  const {navigate} = useNavigation<NavigationProp<ProfileNavigatorStackParamList>>();
  if(!profile) return null;

  return <View style={styles.container}>
    <AvatarField source={profile.avatar}/>
    <View style={styles.profileInfoContainer}>
        <Text style={styles.name}>{profile.name}</Text>
        <View style={styles.emailContainer}>
            <Text style={styles.email}>{profile.email}</Text>
            {profile.verified ? <MaterialIcon name='verified' size={15} color={colors.SECONDARY}/> :
            <Octicons name='unverified' size={15} color={colors.SECONDARY}/>}
        </View>
        <View style={styles.profileActionLinkContainer}>
            <Text style={styles.profileActionLink}>{profile.followers} Followers</Text>
            <Text style={styles.profileActionLink}>{profile.followings} Followings</Text>
        </View>
    </View>
    <Pressable onPress={() => navigate('ProfileSettings')} style={styles.settingsButton}>
        <AntDesign name='setting' size={22} color={colors.CONTRAST}/>
    </Pressable>
  </View>;
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'center'
    },
    name: {
        color: colors.CONTRAST,
        fontSize: 18,
        fontWeight: '700'
    },
    email: {
        color: colors.CONTRAST,
        marginRight: 5
    },
    emailContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    profileInfoContainer: {
       paddingHorizontal: 10 
    },
    profileActionLink: {
        backgroundColor: colors.SECONDARY,
        color: colors.PRIMARY,
        paddingHorizontal: 4,
        paddingVertical: 2,
        margin: 5
    },
    profileActionLinkContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    settingsButton: {
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default ProfileContainer;