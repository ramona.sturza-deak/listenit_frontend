import colors from '@utils/colors';
import {FC, ReactNode} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';

interface Props {
    title: string;
    subtitle?: string;
    children: ReactNode;
}

const AuthFormContainer: FC<Props> = ({children, title, subtitle}) => {
  return <View style={styles.container}>
     <View style={styles.titleContainer}>
        <Image style={styles.logo} source={require('../assets/logo.jpg')}/>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.subtitle}>{subtitle}</Text>
      </View>
      {children}
  </View>;
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.PRIMARY,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 15
    },
    logo: {
        height: 80, 
        width: 80
    },
    title: {
        color: colors.SECONDARY, 
        fontSize: 25, 
        fontWeight: 'bold', 
        paddingVertical: 5
    },
    subtitle: {
        color: colors.CONTRAST, 
        fontSize: 16
    },
    titleContainer: {
        width: '100%', 
        marginBottom: 20
    }
});

export default AuthFormContainer;