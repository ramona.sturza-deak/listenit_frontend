import { NativeStackScreenProps } from '@react-navigation/native-stack';
import PlaylistItem from '@ui/PlaylistItem';
import {FC} from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import { useDispatch } from 'react-redux';
import { Playlist } from 'src/@types/audio';
import { PublicProfileTabParamList } from 'src/@types/navigation';
import { useFetchPublicPlaylists } from 'src/hooks/query';
import { updateAllowPlaylistAudioRemove, updateIsPrivate, updatePlaylistVisibility, updateSelectedListId } from 'src/store/playlistModal';

type Props = NativeStackScreenProps<PublicProfileTabParamList, 'PublicPlaylists'>

const PublicPlaylistsTab: FC<Props> = (props) => {
  const {data} = useFetchPublicPlaylists(props.route.params.profileId);
  const dispatch = useDispatch();
  
  const handleListPress = (playlist: Playlist) => {
    dispatch(updateAllowPlaylistAudioRemove(false));
    dispatch(updateSelectedListId(playlist.id));
    dispatch(updatePlaylistVisibility(true));
    dispatch(updateIsPrivate(playlist.visibility === 'private'));
  }

  return <ScrollView style={styles.container}>
    {data?.map(playlist => {
      return <PlaylistItem onPress={() => handleListPress(playlist)} key={playlist.id} playlist={playlist}/>
    })}
  </ScrollView>;
};

const styles = StyleSheet.create({
    container: {},
});

export default PublicPlaylistsTab;