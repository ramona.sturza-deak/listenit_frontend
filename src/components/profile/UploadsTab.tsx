import React, { FC, useState } from 'react';
import { StyleSheet, Text, Pressable, ScrollView, RefreshControl } from 'react-native';
import AntDesing from 'react-native-vector-icons/AntDesign';
import { useSelector } from 'react-redux';
import { useQueryClient } from 'react-query';
import { NavigationProp, useNavigation } from '@react-navigation/native';

import OptionsModal from '@components/OptionsModal';
import AudioListItem from '@ui/AudioList';
import AudioListLoadingUI from '@ui/AudioListLoading';
import EmptyRecords from '@ui/EmptyRecords';
import colors from '@utils/colors';

import { AudioData } from 'src/@types/audio';
import { ProfileNavigatorStackParamList } from 'src/@types/navigation';
import { useFetchUploadsByProfile } from 'src/hooks/query';
import useAudioController from 'src/hooks/useAudioController';
import { getPlayerState } from 'src/store/player';

interface Props {}

const UploadsTab: FC<Props> = () => {
  const [showOptions, setShowOptions] = useState(false);
  const [selectedAudio, setSelectedAudio] = useState<AudioData>();
  const { onGoingAudio } = useSelector(getPlayerState);
  const { data, isLoading, isFetching } = useFetchUploadsByProfile();
  const { onAudioPress } = useAudioController();
  const { navigate } = useNavigation<NavigationProp<ProfileNavigatorStackParamList>>();

  const handleOnLongPress = (audio: AudioData) => {
    setSelectedAudio(audio);
    setShowOptions(true);
  };

  const handleOnEditPress = () => {
    setShowOptions(false);
    if (selectedAudio) {
      navigate('UpdateAudio', { audio: selectedAudio });
    }
  };

  const queryClient = useQueryClient();
  const handleRefresh = () => {
    queryClient.invalidateQueries({ queryKey: ['uploads-by-profile'] });
  };

  if (isLoading) return <AudioListLoadingUI />;

  if (!data?.length) return <EmptyRecords title="There is no audio!" />;

  return (
    <ScrollView
      style={styles.container}
      refreshControl={
        <RefreshControl
          refreshing={isFetching}
          onRefresh={handleRefresh}
          tintColor={colors.CONTRAST}
        />
      }
    >
      {data?.map(item => (
        <AudioListItem
          onPress={() => onAudioPress(item, data)}
          key={item.id}
          audio={item}
          isPlaying={onGoingAudio?.id === item.id}
          onLongPress={() => handleOnLongPress(item)}
        />
      ))}
      <OptionsModal
        visible={showOptions}
        onRequestClose={() => setShowOptions(false)}
        options={[
          {
            title: 'Edit',
            icon: 'edit',
            onPress: handleOnEditPress,
          },
        ]}
        renderItem={item => (
          <Pressable onPress={item.onPress} style={styles.optionContainer}>
            <AntDesing size={24} color={colors.PRIMARY} name={item.icon} />
            <Text style={styles.optionLabel}>{item.title}</Text>
          </Pressable>
        )}
      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    
  },
  optionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
  },
  optionLabel: {
    color: colors.PRIMARY,
    fontSize: 16,
    marginLeft: 5,
  },
});

export default UploadsTab;