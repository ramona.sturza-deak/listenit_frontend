import AudioList from '@ui/AudioList';
import AudioListLoading from '@ui/AudioListLoading';
import EmptyRecords from '@ui/EmptyRecords';
import colors from '@utils/colors';
import {FC} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {
  GestureHandlerRootView,
  RefreshControl,
} from 'react-native-gesture-handler';
import {useQueryClient} from 'react-query';
import {useSelector} from 'react-redux';
import {useFetchFavorites} from 'src/hooks/query';
import useAudioController from 'src/hooks/useAudioController';
import {getPlayerState} from 'src/store/player';

interface Props {}

const FavoritesTab: FC<Props> = props => {
  const {data, isLoading, isFetching} = useFetchFavorites();
  const {onAudioPress} = useAudioController();
  const {onGoingAudio} = useSelector(getPlayerState);
  const queryClient = useQueryClient();
  const handleRefresh = () => {
    queryClient.invalidateQueries({queryKey: ['favorite']});
  };

  if (isLoading) return <AudioListLoading />;

  return (
    <GestureHandlerRootView>
    <ScrollView
      style={styles.container}
      refreshControl={
        <RefreshControl
          refreshing={isFetching}
          onRefresh={handleRefresh}
          tintColor={colors.CONTRAST}
        />
      }>
      {!data?.length ? <EmptyRecords title="You have no favorites!" /> : null}
      {data?.map(item => {
        return (
          <AudioList
            onPress={() => onAudioPress(item, data)}
            isPlaying={onGoingAudio?.id === item.id}
            key={item.id}
            audio={item}
          />
        );
      })}
    </ScrollView>
    </GestureHandlerRootView>
  );
};

const styles = StyleSheet.create({
  container: {},
});

export default FavoritesTab;
