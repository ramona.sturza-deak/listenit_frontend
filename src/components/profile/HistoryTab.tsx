import AudioListLoading from '@ui/AudioListLoading';
import EmptyRecords from '@ui/EmptyRecords';
import colors from '@utils/colors';
import {FC} from 'react';
import {StyleSheet, Text, View, ScrollView, Pressable} from 'react-native';
import {GestureHandlerRootView, RefreshControl} from 'react-native-gesture-handler';
import {useFetchHistory} from 'src/hooks/query';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {getClient} from 'src/api/client';
import {useQueryClient} from 'react-query';
import {historyAudio} from 'src/@types/audio';

interface Props {}

const HistoryTab: FC<Props> = props => {
  const {data, isLoading, isFetching} = useFetchHistory();
  const queryClient = useQueryClient();
  const noData = !data?.length;

  const removeHistories = async (histories: string[]) => {
    const client = await getClient();
    await client.delete('/history?histories=' + JSON.stringify(histories));
    queryClient.invalidateQueries({queryKey: ['histories']});
  };

  const handleSingleHistoryClear = async (history: historyAudio) => {
    await removeHistories([history.id]);
  };

  const handleRefresh = () => {
    queryClient.invalidateQueries({queryKey: ['histories']});
  };

  if (isLoading) return <AudioListLoading />;

  return (
    <GestureHandlerRootView>
    <ScrollView
      refreshControl={
        <RefreshControl refreshing={isFetching} onRefresh={handleRefresh} />
      }
      style={styles.container}>
      {noData ? <EmptyRecords title="There is no history!" /> : null}
      {data?.map((item, mainIndex) => {
        return (
          <View key={item.date + mainIndex}>
            <Text style={styles.date}>{item.date}</Text>
            <View style={styles.listContainer}>
              {item.audios.map((audio, index) => {
                return (
                  <View key={audio.id + index} style={styles.history}>
                    <Text style={styles.title}>{audio.title}</Text>
                    <Pressable onPress={() => handleSingleHistoryClear(audio)}>
                      <AntDesign name="close" color={colors.CONTRAST} />
                    </Pressable>
                  </View>
                );
              })}
            </View>
          </View>
        );
      })}
    </ScrollView>
    </GestureHandlerRootView>
  );
};

const styles = StyleSheet.create({
  container: {},
  date: {
    color: 'white',
  },
  title: {
    color: colors.CONTRAST,
    paddingHorizontal: 5,
    fontWeight: '700',
    flex: 1,
  },
  history: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.OVERLAY,
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
  },
  listContainer: {
    marginTop: 10,
    paddingLeft: 10,
  },
});

export default HistoryTab;
