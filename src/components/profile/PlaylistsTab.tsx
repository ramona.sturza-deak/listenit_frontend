import OptionsModal from '@components/OptionsModal';
import EmptyRecords from '@ui/EmptyRecords';
import PlaylistItem from '@ui/PlaylistItem';
import { FC, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Playlist } from 'src/@types/audio';
import catchAsyncError from 'src/api/catchError';
import { useFetchPlaylist } from 'src/hooks/query';
import { updateNotification } from 'src/store/notification';
import {
  updateAllowPlaylistAudioRemove,
  updateIsPrivate,
  updatePlaylistVisibility,
  updateSelectedListId,
} from 'src/store/playlistModal';
import AntDesing from 'react-native-vector-icons/AntDesign';
import OptionSelector from '@ui/OptionSelector';
import colors from '@utils/colors';
import PlaylistForm, { PlaylistInfo } from '@components/PlaylistForm';
import deepEqual from 'deep-equal';
import { getClient } from 'src/api/client';
import { useQueryClient } from 'react-query';
import { RefreshControl, ScrollView } from 'react-native';

interface Props {}

const PlaylistTab: FC<Props> = props => {
  const { data, isFetching } = useFetchPlaylist();
  const dispatch = useDispatch();
  const [showOptions, setShowOptions] = useState(false);
  const [updateForm, setUpdateForm] = useState(false);
  const [selectedPlaylist, setSelectedPlaylist] = useState<Playlist>();
  const queryClient = useQueryClient();

  const handleOnListPress = (playlist: Playlist) => {
    dispatch(updateIsPrivate(playlist.visibility === 'private'));
    dispatch(updateAllowPlaylistAudioRemove(true));
    dispatch(updateSelectedListId(playlist.id));
    dispatch(updatePlaylistVisibility(true));
  };

  const handleOnRefresh = () => {
    queryClient.invalidateQueries(['playlist']);
  };

  const updatePlaylist = async (item: Playlist) => {
    try {
      const client = await getClient();
      closeUpdateForm();
      await client.patch('/playlist', item);
      queryClient.invalidateQueries(['playlist']);

      dispatch(
        updateNotification({ message: 'Playlist updated.', type: 'success' }),
      );
    } catch (error) {
      const errorMessage = catchAsyncError(error);
      dispatch(updateNotification({ message: errorMessage, type: 'error' }));
    }
  };

  const handleOnLongPress = (playlist: Playlist) => {
    setSelectedPlaylist({
      ...playlist
    });
    setShowOptions(true);
  };

  const closeOptions = () => {
    setShowOptions(false);
  };

  const closeUpdateForm = () => {
    setUpdateForm(false);
  };

  const handleOnEditPress = () => {
    closeOptions();
    setUpdateForm(true);
  };

  const handleOnDeletePress = async () => {
    try {
      const client = await getClient();
      closeOptions();
      await client.delete('/playlist?all=yes&playlistId=' + selectedPlaylist?.id);
      queryClient.invalidateQueries(['playlist']);

      dispatch(
        updateNotification({ message: 'Playlist deleted.', type: 'success' }),
      );
    } catch (error) {
      const errorMessage = catchAsyncError(error);
      dispatch(updateNotification({ message: errorMessage, type: 'error' }));
    }
  };

  return (
    <>
      <ScrollView
        contentContainerStyle={{ flexGrow: 1 }}
        refreshControl={
          <RefreshControl
            refreshing={isFetching}
            onRefresh={handleOnRefresh}
          />
        }
      >
        {data?.length ? (
          data.map((item, index) => (
            <PlaylistItem
              onPress={() => handleOnListPress(item)}
              key={item.id}
              playlist={item}
              onLongPress={() => handleOnLongPress(item)}
            />
          ))
        ) : (
          <EmptyRecords title="There is no playlist!" />
        )}
      </ScrollView>

      <OptionsModal
        visible={showOptions}
        onRequestClose={closeOptions}
        options={[
          {
            title: 'Edit',
            icon: 'edit',
            onPress: handleOnEditPress,
          },
          {
            title: 'Delete',
            icon: 'delete',
            onPress: handleOnDeletePress,
          },
        ]}
        renderItem={item => {
          return (
            <OptionSelector
              onPress={item.onPress}
              label={item.title}
              icon={
                <AntDesing size={24} color={colors.PRIMARY} name={item.icon} />
              }
            />
          );
        }}
      />
      <PlaylistForm
        visible={updateForm}
        onRequestClose={closeUpdateForm}
        onSubmit={value => {
          const isSame = deepEqual(value, {
            title: selectedPlaylist?.title,
            private: selectedPlaylist?.visibility === 'private'
          })
          if (isSame || !selectedPlaylist) return;
          updatePlaylist({
            ...selectedPlaylist,
            title: value.title,
            visibility: value.private ? 'private' : 'public'
          });
        }}
        initialValue={{
          title: selectedPlaylist?.title || '',
          private: selectedPlaylist?.visibility === 'private'
        }}
      />
    </>
  );
};

export default PlaylistTab;