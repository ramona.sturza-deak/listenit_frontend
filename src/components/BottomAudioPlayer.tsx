import colors from '@utils/colors';
import {FC, useState} from 'react';
import {StyleSheet, View, Image, Text, Pressable} from 'react-native';
import {useSelector} from 'react-redux';
import {getPlayerState} from 'src/store/player';
import AntDesign from 'react-native-vector-icons/AntDesign';
import PlayPauseButton from '@ui/PlayPauseButton';
import useAudioController from 'src/hooks/useAudioController';
import Loader from '@ui/Loader';
import {mapRange} from '@utils/formulas_progressBar';
import {useProgress} from 'react-native-track-player';
import AudioPlayer from './AudioPlayer';
import CurrentAudioList from './CurrentAudioList';
import { useFetchIsFavorite } from 'src/hooks/query';
import { useMutation, useQueryClient } from 'react-query';
import { getClient } from 'src/api/client';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import { HomeNavigatorStackParamList } from 'src/@types/navigation';
import { getAuthState } from 'src/store/auth';

interface Props {}

export const AudioPlayerHeight = 60;

const BottomAudioPlayer: FC<Props> = props => {
  const {onGoingAudio} = useSelector(getPlayerState);
  const {profile} = useSelector(getAuthState);
  const {isPlaying, togglePlayPause, isBusy} = useAudioController();
  const {data: isFav} = useFetchIsFavorite(onGoingAudio?.id || '');
  const progress = useProgress();
  const poster = onGoingAudio?.poster;
  const source = poster ? {uri: poster} : require('../assets/music.png');
  const [playerVisibility, setPlayerVisibility] = useState(false);
  const [showCurrentList, setShowCurrentList] = useState(false);
  const {navigate} = useNavigation<NavigationProp<HomeNavigatorStackParamList>>();

  const closePlayerModal = () => {
    setPlayerVisibility(false);
  };
  const showPlayerModal = () => {
    setPlayerVisibility(true);
  };
  const handleCloseCurrentList = () => {
    setShowCurrentList(false);
  };
  const handleListOptionPress = () => {
    closePlayerModal();
    setShowCurrentList(true);
  };
  const handleProfilePress = () => {
    closePlayerModal();
    if(profile?.id === onGoingAudio?.owner.id) {
      navigate('Profile');
    } else {
    navigate('PublicProfile', {
      profileId: onGoingAudio?.owner.id || ''
    });
  }
  };

  const queryClient = useQueryClient();
  const toggleIsFav = async (id: string) => {
    if(!id) return;
    const client = await getClient();
    await client.post('/favorite?audioId=' + id)
  }

  const favoriteMutation = useMutation({
    mutationFn: async id => toggleIsFav(id),
    onMutate: (id: string) => {
      queryClient.setQueryData<boolean>(
        ['favorite', onGoingAudio?.id],
        oldData => !oldData,
      );
    },
  });

  return (
    <>
      <View
        style={{
          height: 2,
          backgroundColor: 'white',
          width: `${mapRange({
            outputMin: 0,
            outputMax: 100,
            inputMin: 0,
            inputMax: progress.duration,
            inputValue: progress.position,
          })}%`,
        }}
      />
      <View style={styles.container}>
        <Image source={source} style={styles.poster} />
        <Pressable onPress={showPlayerModal} style={styles.contentContainer}>
          <Text style={styles.title}>{onGoingAudio?.title}</Text>
          <Text style={styles.name}>{onGoingAudio?.owner.name}</Text>
        </Pressable>
        <Pressable style={{paddingHorizontal: 10}} onPress={() =>  favoriteMutation.mutate(onGoingAudio?.id || '')}>
          <AntDesign name={isFav ? "heart" : "hearto"} size={24} color={colors.CONTRAST} />
        </Pressable>
        {isBusy ? (
          <Loader />
        ) : (
          <PlayPauseButton playing={isPlaying} onPress={togglePlayPause} />
        )}
      </View>
      <AudioPlayer
        visible={playerVisibility}
        onRequestClose={closePlayerModal}
        onListOptionPress={handleListOptionPress}
        onProfileLinkPress={handleProfilePress}
      />
      <CurrentAudioList
        visible={showCurrentList}
        onRequestClose={handleCloseCurrentList}
      />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: AudioPlayerHeight,
    backgroundColor: 'rgba(0,0,0,0.5)',
    padding: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  poster: {
    height: AudioPlayerHeight - 10,
    width: AudioPlayerHeight - 10,
    borderRadius: 5,
  },
  title: {
    color: colors.CONTRAST,
    fontWeight: '700',
    paddingHorizontal: 5,
  },
  contentContainer: {
    flex: 1,
    height: '100%',
    padding: 5,
  },
  name: {
    color: colors.SECONDARY,
    fontWeight: '700',
    paddingHorizontal: 5,
  }
});

export default BottomAudioPlayer;