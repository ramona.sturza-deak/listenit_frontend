import {Formik, FormikHelpers} from 'formik';
import {ReactNode} from 'react';

interface Props<T> {
  initialValues: any;
  signupValidation: any;
  onSubmit(values: T, formikHelpers: FormikHelpers<T>): void
  children: ReactNode
}

const Form = <T extends object>(props: Props<T>) => {
  return (
    <Formik
      onSubmit={props.onSubmit}
      initialValues={props.initialValues}
      validationSchema={props.signupValidation}>
      {props.children}
    </Formik>
  );
};

export default Form;