import {FC} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import UploadTab from '@components/profile/UploadsTab';
import PlaylistsTab from '@components/profile/PlaylistsTab';
import FavoritesTab from '@components/profile/FavoritesTab';
import HistoryTab from '@components/profile/HistoryTab';
import colors from '@utils/colors';
import ProfileContainer from '@components/ProfileContainer';
import { useSelector } from 'react-redux';
import { getAuthState } from 'src/store/auth';
import AppView from '@components/AppView';

const Tab = createMaterialTopTabNavigator();

interface Props {}

const Profile: FC<Props> = props => {
  const {profile} = useSelector(getAuthState);

  return (
    <AppView>
    <View style={styles.container}>
      <ProfileContainer profile={profile}/>
      <Tab.Navigator screenOptions={{
        tabBarStyle: styles.tabBar,
        tabBarLabelStyle: styles.tabBarLabel
      }}>
        <Tab.Screen name='Uploads' component={UploadTab}/>
        <Tab.Screen name='Playlists' component={PlaylistsTab}/>
        <Tab.Screen name='Favorites' component={FavoritesTab}/>
        <Tab.Screen name='History' component={HistoryTab}/>
      </Tab.Navigator>
    </View>
    </AppView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  tabBar: {
    backgroundColor: 'transparent',
    elevation: 0,
    marginBottom: 20
  },
  tabBarLabel: {
    color: colors.CONTRAST,
    fontSize: 12
  }
});

export default Profile;
