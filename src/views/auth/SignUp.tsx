import AuthInputField from '@components/form/AuthInputField';
import Form from '@components/form';
import {FC, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import * as yup from 'yup';
import SubmitBtn from '@components/form/SubmitBtn';
import PasswordVisibilityIcon from '@ui/PasswordVisibilityIcon';
import AppLink from '@ui/AppLink';
import AuthFormContainer from '@components/AuthFormContainer';
import { AuthStackParamList } from 'src/@types/navigation';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import { FormikHelpers } from 'formik';
import client from 'src/api/client';
import { isAxiosError } from 'axios';
import catchAsyncError from 'src/api/catchError';
import { useDispatch } from 'react-redux';
import { updateNotification } from 'src/store/notification';

const signupValidation = yup.object({
  name: yup
    .string()
    .trim('Name is missing!')
    .min(3, 'Name is too short!')
    .required('Name is required!'),
  email: yup
    .string()
    .trim('Email is missing!')
    .email('Invalid email!')
    .required('Email is required!'),
  password: yup
    .string()
    .trim('Password is missing!')
    .min(8, 'Password is too short!')
    .matches(
      /^(?=.*[a-zA-Z])(?=.*\d)(?=.*[!@#\$%\^&\*])[a-zA-Z\d!@#\$%\^&\*]+$/,
      'Password is too simple!',
    )
    .required('Password is required!'),
});

interface Props {}

interface NewUser {
  name: string;
  email: string;
  password: string;
}

const initialValues = {
  name: '',
  email: '',
  password: ''
};

const SignUp: FC<Props> = props => {
  const [secureEntry, setSecureEntry] = useState(true);
  const togglePassword = () => {
    setSecureEntry(!secureEntry)
  }
  const dispatch = useDispatch();

  const handleSubmit = async (values: NewUser, actions: FormikHelpers<NewUser>) => {
    actions.setSubmitting(true);
    try {
      //send info to API
      const {data} = await client.post('/auth/create', {...values});
      navigation.navigate('Verification', {userInfo: data.user});
      console.log(data);
    } catch (error) {
      const errorMessage = catchAsyncError(error);
      dispatch(updateNotification({message: errorMessage, type: 'error'}));
    }
    actions.setSubmitting(false);
  }
  const navigation = useNavigation<NavigationProp<AuthStackParamList>>();

  return (
      <Form
        onSubmit={handleSubmit}
        initialValues={initialValues}
        signupValidation={signupValidation}>
        <AuthFormContainer title='Welcome!' subtitle="Let's get started by creating your account.">
          <View style={styles.formContainer}>
          <AuthInputField
            name="name"
            placeholder="John Doe"
            label="Name"
            containerStyle={styles.marginBottom}
          />
          <AuthInputField
            name="email"
            placeholder="john@email.com"
            label="Email"
            keyboardType="email-address"
            autoCapitalize="none"
            containerStyle={styles.marginBottom}
          />
          <AuthInputField
            name="password"
            placeholder="********"
            label="Password"
            autoCapitalize="none"
            secureTextEntry={secureEntry}
            containerStyle={styles.marginBottom}
            rightIcon={<PasswordVisibilityIcon privateIcon={secureEntry}/>}
            onRightIconPress={togglePassword}
          />
          <SubmitBtn title='Sign Up'/>
          <View style={styles.linksContainer}>
            <AppLink link="I Forgot My Password" onPress={() => {
              navigation.navigate("LostPassword")
            }}/>
            <AppLink link="Sign In" onPress={() => {
              navigation.navigate("SignIn")
            }}/>
          </View>
          </View>
        </AuthFormContainer>
      </Form>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    width: '100%'
  },
  marginBottom: {
    marginBottom: 20,
  },
  linksContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 20
  }
});

export default SignUp;