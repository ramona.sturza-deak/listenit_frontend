import React, { FC, useEffect, useRef, useState } from 'react';
import { Pressable, StyleSheet, Text, TextInput, View } from 'react-native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { AuthStackParamList, ProfileNavigatorStackParamList } from 'src/@types/navigation';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import AppButton from '@ui/AppButon';
import OTPField from '@ui/OTPField';
import AuthFormContainer from '@components/AuthFormContainer';
import ReVerificationLink from '@components/ReVerificationLink';
import client from 'src/api/client';
import colors from '@utils/colors';
import catchAsyncError from 'src/api/catchError';
import { updateNotification } from 'src/store/notification';

type Props = NativeStackScreenProps<AuthStackParamList | ProfileNavigatorStackParamList, "Verification">;

const otpFields = new Array(6).fill('');

const Verification: FC<Props> = ({ route }) => {
  const [otp, setOtp] = useState([...otpFields]);
  const [activeOtpIndex, setActiveOtpIndex] = useState(0);
  const [submitting, setSubmitting] = useState(false);
  const { userInfo } = route.params;
  const inputRef = useRef<TextInput>(null);
  const dispatch = useDispatch();
  const navigation = useNavigation<NavigationProp<any>>();

  const handleChange = (value: string, index: number) => {
    const newOtp = [...otp];

    if (value === 'Backspace') {
      // Move back
      if (!newOtp[index]) setActiveOtpIndex(index - 1);
      newOtp[index] = '';
    } else {
      // Move next
      setActiveOtpIndex(index + 1);
      newOtp[index] = value;
    }

    setOtp([...newOtp]);
  };

  // Check if OTP is valid
  const isValidOTP = otp.every(value => {
    return value.trim();
  });

  const handleSubmit = async () => {
    if (!isValidOTP) return dispatch(updateNotification({ message: 'Invalid OTP!', type: 'error' }));
    setSubmitting(true);
    try {
      const { data } = await client.post('/auth/verify-email', { userId: userInfo.id, token: otp.join('') });

      dispatch(updateNotification({ message: data.message, type: 'success' }));

      if (navigation.getState().routeNames.includes('SignIn')) {
        // Navigate back to SignIn
        navigation.navigate('SignIn');
      }

      if (navigation.getState().routeNames.includes('ProfileSettings')) {
        // Navigate back to ProfileSettings
        navigation.navigate('ProfileSettings');
      }

    } catch (error) {
      const errorMessage = catchAsyncError(error);
      dispatch(updateNotification({ message: errorMessage, type: 'error' }));
    }
    setSubmitting(false);
  };

  useEffect(() => {
    inputRef.current?.focus();
  }, [activeOtpIndex]);

  const goBack = () => {
    const routes = navigation.getState().routes;
    const previousRoute = routes[routes.length - 2];

    if (previousRoute && previousRoute.name === 'SignUp') {
      navigation.navigate('SignIn');
    } else {
      navigation.goBack();
    }
  };

  return (
    <AuthFormContainer title="Please check your email.">
      <Pressable onPress={goBack} style={styles.signInButton}>
        <Text style={styles.signInText}>Go Back</Text>
      </Pressable>
      <View style={styles.inputContainer}>
        {otpFields.map((_, index) => {
          return (
            <OTPField
              ref={activeOtpIndex === index ? inputRef : null}
              key={index}
              placeholder="*"
              onKeyPress={({ nativeEvent }) => {
                handleChange(nativeEvent.key, index);
              }}
              keyboardType="numeric"
              value={otp[index] || ""}
            />
          );
        })}
      </View>

      <AppButton busy={submitting} title="Submit" onPress={handleSubmit} />

      <View style={styles.linkContainer}>
        <ReVerificationLink linkTitle='Re-send OTP' userId={userInfo.id} />
      </View>
    </AuthFormContainer>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
  },
  linkContainer: {
    marginTop: 20,
    width: '100%',
    justifyContent: 'flex-end',
  },
  signInButton: {
    position: 'absolute',
    top: 20,
    left: 20,
    zIndex: 1,
  },
  signInText: {
    color: 'white',
    backgroundColor: colors.SECONDARY,
    padding: 8,
    borderRadius: 5,
  },
});

export default Verification;