import AuthInputField from '@components/form/AuthInputField';
import Form from '@components/form';
import {FC} from 'react';
import {StyleSheet, View} from 'react-native';
import * as yup from 'yup';
import SubmitBtn from '@components/form/SubmitBtn';
import AppLink from '@ui/AppLink';
import AuthFormContainer from '@components/AuthFormContainer';
import { AuthStackParamList } from 'src/@types/navigation';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import { FormikHelpers } from 'formik';
import client from 'src/api/client';
import catchAsyncError from 'src/api/catchError';
import { updateNotification } from 'src/store/notification';
import { useDispatch } from 'react-redux';

const lostPasswordValidation = yup.object({
  email: yup
    .string()
    .trim('Email is missing!')
    .email('Invalid email!')
    .required('Email is required!')
});

interface Props {}

interface initialValue {
  email: ''
}

const initialValues = {
  email: ''
};

const LostPassword: FC<Props> = props => {
  const dispatch = useDispatch();
  const navigation = useNavigation<NavigationProp<AuthStackParamList>>();

  const handleSubmit = async (values: initialValue, actions: FormikHelpers<initialValue>) => {
    actions.setSubmitting(true);
    try {
      //send info to API
      const {data} = await client.post('/auth/forget-password', {...values});
      console.log(data);
    } catch (error) {
      const errorMessage = catchAsyncError(error);
      dispatch(updateNotification({message: errorMessage, type: 'error'}));
    }
    actions.setSubmitting(false);
  }

  return (
      <Form
        onSubmit={handleSubmit}
        initialValues={initialValues}
        signupValidation={lostPasswordValidation}>
        <AuthFormContainer title='Forgot Password!' subtitle="Don't worry, we'll help you get your password back.">
          <View style={styles.formContainer}>
          <AuthInputField
            name="email"
            placeholder="john@email.com"
            label="Email"
            keyboardType="email-address"
            autoCapitalize="none"
            containerStyle={styles.marginBottom}
          />
          <SubmitBtn title='Send link'/>
          <View style={styles.linksContainer}>
            <AppLink link="Sign In" onPress={() => {
              navigation.navigate("SignIn")
            }}/>
            <AppLink link="Sign Up" onPress={() => {
              navigation.navigate("SignUp")
            }}/>
          </View>
          </View>
        </AuthFormContainer>
      </Form>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    width: '100%'
  },
  marginBottom: {
    marginBottom: 20,
  },
  linksContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 20
  }
});

export default LostPassword;