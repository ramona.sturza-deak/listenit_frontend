import {PayloadAction, createSelector, createSlice} from '@reduxjs/toolkit';
import {RootState} from '.';

interface PlaylistModal {
  visible: boolean;
  selectedListId?: string;
  isPrivate?: boolean;
  allowPlaylistAudioRemove?: boolean;
}

const initialState: PlaylistModal = {
  visible: false,
};

const slice = createSlice({
  name: 'playlistModal',
  initialState,
  reducers: {
    updatePlaylistVisibility(
      playerState,
      {payload}: PayloadAction<boolean>,
    ) {
      playerState.visible = payload;
    },
    updateSelectedListId(playerState, {payload}: PayloadAction<string>) {
      playerState.selectedListId = payload;
    },
    updateIsPrivate(playerState, {payload}: PayloadAction<boolean>) {
      playerState.isPrivate = payload;
    },
    updateAllowPlaylistAudioRemove(playerState, {payload}: PayloadAction<boolean>) {
      playerState.allowPlaylistAudioRemove = payload;
    }
  },
});

export const getPlaylistModalState = createSelector(
  (state: RootState) => state.playlistModal,
  modalState => ({
    visible: modalState.visible,
    selectedListId: modalState.selectedListId,
    isPrivate: modalState.isPrivate,
    allowPlaylistAudioRemove: modalState.allowPlaylistAudioRemove
  }),
);

export const {updatePlaylistVisibility, updateSelectedListId, updateIsPrivate, updateAllowPlaylistAudioRemove} = slice.actions;

export default slice.reducer;