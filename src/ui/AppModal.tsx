import colors from '@utils/colors';
import {FC, ReactNode, useEffect} from 'react';
import {Dimensions} from 'react-native';
import {Pressable} from 'react-native';
import {Modal, StyleSheet} from 'react-native';
import {
  GestureDetector,
  GestureHandlerRootView,
  Gesture,
} from 'react-native-gesture-handler';
import Animated, {
  runOnJS,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';

interface Props {
  children: ReactNode;
  visible: boolean;
  onRequestClose(): void;
  animation?: boolean;
}

const {height} = Dimensions.get('window');
const modalHeight = height - 150;

const AppModal: FC<Props> = ({
  children,
  visible,
  onRequestClose,
  animation,
}) => {
  const translateY = useSharedValue(modalHeight);
  const translateStyle = useAnimatedStyle(() => ({
    transform: [{translateY: translateY.value}],
  }));
  const handleClose = () => {
    translateY.value = modalHeight;
    onRequestClose();
  };
  const gesture = Gesture.Pan()
    .onUpdate(event => {
      if (event.translationY <= 0) return;
      translateY.value = event.translationY;
    })
    .onFinalize(event => {
      if (event.translationY <= modalHeight / 2)
        translateY.value = 0; //show all modal, 0 is default value
      else {
        translateY.value = event.translationY; //show only a part => hide
        runOnJS(handleClose)();
      }
    });
  useEffect(() => {
    if (visible) translateY.value = withTiming(0);
  }, [visible, animation]);

  return (
    <Modal onRequestClose={handleClose} visible={visible} transparent>
      <GestureHandlerRootView style={{flex: 1}}>
        <Pressable onResponderEnd={handleClose} style={styles.backdrop} />
        <Animated.View style={[styles.modal, translateStyle]}>
          <GestureDetector gesture={gesture}>
            <Animated.View style={styles.handle} />
          </GestureDetector>
          {children}
        </Animated.View>
      </GestureHandlerRootView>
    </Modal>
  );
};

const styles = StyleSheet.create({
  backdrop: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: colors.INACTIVE_CONTRAST,
  },
  modal: {
    backgroundColor: colors.PRIMARY,
    height: modalHeight,
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    borderTopEndRadius: 10,
    borderTopStartRadius: 10,
    overflow: 'hidden',
  },
  handle: {
    width: '100%',
    height: 45,
    backgroundColor: 'transparent',
    position: 'absolute',
    top: 0,
    zIndex: 1,
  }
});

export default AppModal;