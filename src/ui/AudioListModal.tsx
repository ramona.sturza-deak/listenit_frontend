import colors from '@utils/colors';
import {FC} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import AppModal from './AppModal';
import {AudioData} from 'src/@types/audio';
import AudioList from './AudioList';
import AudioListLoading from './AudioListLoading';
import { useSelector } from 'react-redux';
import { getPlayerState } from 'src/store/player';

interface Props {
  title?: string;
  visible: boolean;
  onRequestClose(): void;
  data: AudioData[];
  loading?: boolean;
  onItemPress(item: AudioData, data: AudioData[]): void;
}

const AudioListModal: FC<Props> = ({title, visible, onRequestClose, data, loading, onItemPress}) => {
  const {onGoingAudio} = useSelector(getPlayerState);

  return (
    <AppModal visible={visible} onRequestClose={onRequestClose}>
      <View style={styles.container}>
      {loading ? <AudioListLoading/> : 
        (
        <>
        <Text style={styles.title}>{title}</Text>
        <FlatList
          contentContainerStyle={styles.flatList}
          data={data}
          keyExtractor={(item) => item.id}
          renderItem={({item}) => {
            return (
                <AudioList 
                audio={item} 
                onPress={() => onItemPress(item, data)}
                isPlaying={onGoingAudio?.id === item.id}
                />
            );
          }}
        />
        </>
        )}
      </View>
    </AppModal>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    color: colors.CONTRAST,
    paddingVertical: 10,
  },
  container: {
    padding: 10,
  },
  flatList: {
    paddingBottom: 50
  }
});

export default AudioListModal;