import colors from '@utils/colors';
import React, {FC} from 'react';
import {StyleSheet, View, Image} from 'react-native';
import EntypoIcon from 'react-native-vector-icons/Entypo';

interface Props {
    source?: string;
}

const AvatarField: FC<Props> = ({source}) => {
  return <View>
    {source ? <Image source={{uri: source}} style={styles.avatar}/> : 
    <View style={styles.avatar}>
        <EntypoIcon name='mic' size={30} color={colors.PRIMARY}/>
    </View>
    }
  </View>;
};

const styles = StyleSheet.create({
    avatar: {
        width: 70,
        height: 70,
        borderRadius: 35,
        backgroundColor: colors.SECONDARY,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 4,
        borderColor: colors.CONTRAST
    }
});

export default AvatarField;