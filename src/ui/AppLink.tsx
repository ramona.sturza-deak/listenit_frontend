import colors from '@utils/colors';
import {FC} from 'react';
import {Pressable, StyleSheet, Text} from 'react-native';

interface Props {
  link: string; 
  onPress?(): void;
  active?: boolean;
}

const AppLink: FC<Props> = ({link, active=true, onPress}) => {
  return <Pressable onPress={active ? onPress : null} style={{opacity: active ? 1: 0.4}}>
    <Text style={styles.link}>{link}</Text>
  </Pressable>;
};

const styles = StyleSheet.create({
    link: {
        color: colors.SECONDARY
    },
});

export default AppLink;