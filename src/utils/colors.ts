const SECONDARY = '#FFBA00';
const PRIMARY = '#0C3B2E';
const CONTRAST = 'white';
const INACTIVE_CONTRAST = 'rgba(255, 255, 255, 0.7)';
const ERROR = '#EF5350';
const SUCCESS = '#D0F0C0';
const OVERLAY = 'rgba(251, 193, 1, 0.6)';

const colors = {
  PRIMARY,
  SECONDARY,
  CONTRAST,
  INACTIVE_CONTRAST,
  ERROR,
  SUCCESS,
  OVERLAY,
};

export default colors;